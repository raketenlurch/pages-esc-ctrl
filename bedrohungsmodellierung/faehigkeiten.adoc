# Fähigkeiten
:toc:

++++
<style>
.warning {
background-color: #7a2518a1;}

</style>

++++

[.warning]
**Diese Sammlung befindet sich gerade noch im Aufbau. Mit der Zeit sollen hier immer mehr Fähigkeiten gelistet werden. Diese Sammlung dient vorallem als grobe Übersicht. Genauere Infos siehe Quellenangabe.**

## Fähigkeit 1: Handy abhören

**Methode:** Da Mobilelefone bereits mit Mikrofon und Sender ausgestattet sind, können sie relativ leicht als Wanzen eingesetzt werden. 

**Gefährdete Assets:** Kommunikation

**Hinweise zur Wahrscheinlichkeit:**  Es gibt keine Belege, dass sowas in der BRD überhaupt gemacht wurde, eine verbreitete Praxis ist es jedenfalls nicht. 

**Fallbeispiele:**  -

**Quelen:**

- http://www.datenschmutz.de/moin/%C3%9Cberwachungstechnik#Lawful_Interception

## Fähigkeit 2: Hubschrauber mit Kameras

**Methode:** Laut dem Polizeibericht 2010 können, die an den Aufklärungshubschraubern befestigten Kameras, schon aus mehreren 100 m Höhe Potrait-Aufnahmen anfertigen. Bei Dunkelheit werden Nachtsichtkameras oder Infrarotkameras verwendet. Letztere messen die Wärmestrahlung des Körpers, können daher nur Menschen mit einer gewissen Körperstatur identifizieren. Dafür können sie allerdings auch durch Büsche sehen. Nachtbildkameras verstärken dagegen das vorhandene Licht, bei wirklicher Dunkelheit sind sie daher nutzlos. In einer Stadt mit Straßenbeleuchtung sind sie dagegen recht wirkungsvoll.

**Gefährdete Assets:** Aktionsablauf, Position

**Hinweise zur Wahrscheinlichkeit:**  -

**Fallbeispiele:** -

**Quellen:**

- http://www.datenschmutz.de/moin/%C3%9Cberwachungstechnik


## Fähigkeit 3: Observation mit Kleinflugzeugen

**Methode:**  Ein Kleinflugzeug mit Kameratechnik

**Gefährdete Assets:** Aktionsablauf, Position

**Hinweise zur Wahrscheinlichkeit:** Laut einem Artikel in der Leipziger Volkszeitung hat das BfV ein Kleinflugzeug zum Auffinden von untergetauchten Neonazis eingesetzt. Es ist anzunehmen, dass der VS bei Linken schon bei geringerem Anlass Kleinflugzeuge zur Unterstützung der Observation einsetzt.

**Fallbeispiele:**  -

**Quellen:**

- http://www.datenschmutz.de/moin/%C3%9Cberwachungstechnik
- http://www.lvz-online.de/nachrichten/mitteldeutschland/streit-um-geheimbericht-zu-neonazi-trio--verbindungen-zu-saechsischer-bloodhonour-sektion/r-mitteldeutschland-a-121683.html

## Fähigkeit 4: Wohnraum Überwachung per Wanze

**Methode:** Zur Aufzeichnung von Geräuschen in Räumen werden verschiedene Geräte eingesetzt, im einfachsten Fall ein schlichtes Mikrofon mit Sender; da diese allerdings mit verschiedenen "Wanzendetektoren" relativ einfach zu lokalsieren sind, werden zunehmend Geräte eingesetzt, die die Signale zunächst aufzeichnen und dann komprimiert übertragen. 

**Gefährdete Assets:** Kommunikation

**Hinweise zur Wahrscheinlichkeit:** Das BMJ berichtet für 2006 (nach dem etwas beschränkenden BVerfG-Beschluss) von 7 überwachten Objekten mit 27 Betroffenen. 

**Fallbeispiele:**  -

**Quellen:**

- http://www.datenschmutz.de/moin/Überwachungstechnik
- http://www.bmj.de/files/-/1319/Bericht%20Wohnraum%C3%BCberwachung_270906.pdf


## Fähigkeit 5: Akustische Überwachung außerhalb von Wohnungen

**Methode:** Außerhalb von Wohnungen erlaubt im Strafverfahren §100f die verdeckte Tonaufzeichnung grob unter den Bedingungen des Telefonabhörens. Nicht Beschuldigte dürfen im Rahmen einer Verhältnismäßigkeitsabwägung in Mitleidenschaft gezogen werden. Entsprechende Regelungen finden sich in vielen Polizeigesetzen zur Gefahrenabwehr (z.B. §22 PolG BaWü).
Geräte können sein: Wanzen, Richtmikrofone und "Audio Scope System" (es erlaubt durch eine Art inverser Wellenfeldsynthese, auf einzelne Schallquellen zu "zoomen", ggf. auch im Nachhinein). 

**Gefährdete Assets:** Kommunikation

**Hinweise zur Wahrscheinlichkeit:** Das BMJ berichtet für 2006 (nach dem etwas beschränkenden BVerfG-Beschluss) von 7 überwachten Objekten mit 27 Betroffenen. 

**Fallbeispiele:**  -

**Quellen:**

- http://www.datenschmutz.de/moin/Überwachungstechnik
- http://www.bmj.de/files/-/1319/Bericht%20Wohnraum%C3%BCberwachung_270906.pdf


## Fähigkeit 6: Direktes Abhören von GSM-Verbindungen

**Methode:** Die Verschlüsselung auf der Luftschnittstelle von GSM kann inzwischen als gebrochen angesehen werden. Bei legaler Überwachung dürfte das keine Rolle spielen, da die Sicherheitsbehörden problemlos eine Telefonüberwachung mittels LawfulInterception einleiten können.
Bei halblegalen bis illegalen Überwachungen allerdings schon. So tauchen inzwischen kommerzielle Lösungen auf, die -- wohl vor allem für Private oder Geheimdienste außerhalb ihrer Jurisdiktion -- das Abhören von GSM-Telefonaten über die Luftschnittstelle erlauben; Ein Whitepaper zu einem solchen Gerät der Firma alarm.de wurde 2002 noch für 200000 Dollar verkauft. Inzwischen sind weit billigere Lösungen verfügbar. 

**Gefährdete Assets:** Kommunikation

**Hinweise zur Wahrscheinlichkeit:**  -

**Fallbeispiele:**  -

**Quellen:**
- http://www.datenschmutz.de/moin/Überwachungstechnik


## Fähigkeit 7: GPS Peilsender

**Methode:** Mini Peilsender werden für jederman/frau verkauft, d.h. auch zum Überwachen von untreuen Lebenspartnern. Die Position wird per GPS bestimmt und per SMS mitgeteilt. Da das Senden von SMS nicht unbemerkt bleibt, lässt sich so etwas durch einen Wanzendetektor finden.
Das Programm zur Observation mit Hilfe eines GPS-Peilsenders heißt Patras und wird von der Bundespolizei, BKA, Zoll und den LKAs der Länder verwendet. Zumindestens geht das aus einem Hack der NoName Crew hervor, welche laut Spiegel Juli 2011 den Server des Zolls für die Observation per GPS gehackt hatte. Danach laden sich das die Observierer (d.h. MEKs) der Polizei das Programm auf einem Laptop und mit Hilfe einer festen GPRS-Verbindung werden die Daten über einen zentralen Server übertragen. Die Peilsender selber senden die Daten mit Hilfe von SMS über ein PAIP (Police Applications Intercommunication Protocol), das PAIP wird auch bei Wanzen und ähnlichem zur Übertragung verwendet (vgl Indymedia)

**Gefährdete Assets:** Position, Bewegungsprofil

**Hinweise zur Wahrscheinlichkeit:**  -

**Fallbeispiele:**  -

**Quellen:**
- http://www.datenschmutz.de/moin/Überwachungstechnik

## Fähigkeit 8: IMSI Catcher

**Methode:** IMSI-Catcher sind kleine inoffizielle GSM-Funkzellen. Sie spiegeln Mobiltelefonen in der unmittelbaren Umgebung (10 bis 100 Metern) einen starken Sendemast vor. Telefone, die nicht vom Netz gemanagt werden (also im Groben solche, mit denen gerade nicht telefoniert wird) versuchen sich daraufhin, sich auf die "gefälschte" Funkzelle einzubuchen. Da dies einen Location Update bewirkt, kann der IMSI-Catcher die IMSI (also die Teilnehmerkennung) der im betroffenen Netz aktiven Telefone auslesen. Auch das Mithören von Mobilfunktelefonaten ist möglich. Dabei werden allerdings auch Daten Unbeteiligter im Funknetzbereich des IMSI-Catchers erfasst, ohne dass diese es erfahren. 

**Gefährdete Assets:** Position, Identität

**Hinweise zur Wahrscheinlichkeit:**  -

**Fallbeispiele:**  -

**Quellen:**

- http://www.datenschmutz.de/moin/IMSI-Catcher

## Fähigkeit 9: WLAN Catcher

**Methode:** Es ist nicht wirklich klar, was das BKA da gekauft hat. Vorstellbar wäre ein Gerät, das eine WLAN-Verbindung anbietet (vielleicht mit einer SSID "Free WLAN" oder so, wie sie von privatrechtlichen Bösewichtern gerne an Flughäfen eingesetzt werden) und hofft, ein Rechner würde sich automatisch damit assoziieren, um dann abzuhören, was die Maschine zu sagen hat. Dagegen würde aber die Prosa zur SSID sprechen.
Denkbar wäre auch, dass das BKA versucht, existierende Funkzellen zu überbrüllen, um Rechner auf ihren Access Point zu zwingen und so abzuhören, was die Maschine so kommuniziert. Das allerdings ist sehr wacklig, funktioniert mit verschlüsselten Netzwerken nur, wenn der Rechner sehr unvorsichtig konfiguriert ist und hat ohnehin wohl wenig Wert, weil ja entsprechende Daten in der Regel vom Provider abgegriffen werden können.
In Summe liegt die Vermutung nahe, dass jemand dem Staat stark überteuerte Mobilrechner andreht.

**Gefährdete Assets:** Koomunikation, Identität

**Hinweise zur Wahrscheinlichkeit:** In Bundestags-Drucksache 17/8544 erwähnt die Bundesregierung, das BKA habe zwischen 2007 und 2011 16 Mal einen "WLAN-Catcher" eingesetzt (die anderen Bundesbehörden haben sich sowas nicht aufschwatzen lassen).

**Fallbeispiele:**  -

**Quellen:**

- http://www.datenschmutz.de/moin/Überwachungstechnik
- http://dip21.bundestag.de/dip21/btd/17/085/1708544.pdf

## Fähigkeit 11: Kameras am Arbeitsplatz

**Methode:** Die am Arbeitsplatz eingesetzten Kameras sind Mini-Kameras und senden ähnlich wie Wanzen per Funk ihre Aufzeichnungen. Sie lassen sich daher auch mit einem Wanzendetektor aufspüren. Kameras, die über eine interne Speicherkarte verfügen, lassen sich auf Grund der Reflektion des Objektivs der Kamera aufspüren (Allerdings nur für Geübte). 

**Gefährdete Assets:** Kommunikation, Bewegugsprofil

**Hinweise zur Wahrscheinlichkeit:**  -

**Fallbeispiele:**  -

**Quellen:**

- http://www.datenschmutz.de/moin/Überwachungstechnik

## Fähigkeit 12: Kameras vor der Haustür

**Methode:** Aus den Akten von etlichen 129a Verfahren geht hervor, dass Kameras vor den Hauseingängen angebracht wurden. Inwieweit es sich dabei um Mini-Kameras oder um Kameras in benachbarten Wohnungen handelt, geht aus den Akten nicht hervor. Mini-Kameras lassen sich in der Regel auch durch Wanzendetektoren aufspüren. Desweiteren gibt es noch die Methode mit Hilfe eines Spiegel nach einem Objektiv zu suchen, da Kameras zumindestens ein Mini-Objektiv haben müssen.

**Gefährdete Assets:** Bewegungsprofil, Zuordnung zu Gruppen

**Hinweise zur Wahrscheinlichkeit:** Im Juli 2011 haben laut Indymedia (mit Fotos) Bewohner neben eines ehemaligen Besetzten Hauses in Berlin, welches vor kurzem geräumt wurde, Kameras in den Dachfenstern der gegenüberliegenden Schule entdeckt. 

**Fallbeispiele:**  -

**Quellen:**

- http://www.datenschmutz.de/moin/Überwachungstechnik


## Fähigkeit 13: Kameras vor der Haustür

**Methode:** Nach dem neuen BKA-Gesetz, darf das BKA auch Kameras in Wohnungen installieren.

**Gefährdete Assets:** Kommunikation, Zuordnung zu Gruppen

**Hinweise zur Wahrscheinlichkeit:**  In Belgien ist das anscheinend schon länger erlaubt, denn die haben 2011 eine Kamera hinter der Lüftung in ihrer Küche gefunden. Auf Indymedia Belgien wird beschrieben, wie sie aussieht und wo sie angebracht wurde. 

**Fallbeispiele:**  -

**Quellen:**

- http://www.datenschmutz.de/moin/Überwachungstechnik
- http://ovl.indymedia.org/news/2011/05/31487.php

## Fähigkeit 14: Drohnen mit Kameras

**Methode:** Drohnen mit Kameras schließen ein Lücke zwischen Hubschraubern und Observierern auf der Erde.

**Gefährdete Assets:** Position,  Ablauf der Aktion

**Hinweise zur Wahrscheinlichkeit:**  Die Polizei von Sachsen nutzt seit 2011 offiziell Drohnen mit Kameras zur Aufklärung. Mehrere Länder testen sie seit ein paar Jahren und auch in Berlin sind sie schon gesichtet worden.

**Fallbeispiele:**  -

**Quellen:**

- http://www.datenschmutz.de/moin/Überwachungstechnik

## Fähigkeit 15: Quellen TKÜ (Staatstrojaner/Onlinedurchsuchung)

**Methode:** Ein Staatstrojaner ist ein Spionage-Programm, welches heimlich auf dem Rechner des Betroffenen von den Sicherheitsbehörden installiert wird, um dessen Computeraktivitäten auszuspionieren. Bekannt ist das die Software verwendet wurde um zu versuchen Skype gespräche mit zu hören.

**Gefährdete Assets:** Kommunikation, Geheimnisse

**Hinweise zur Wahrscheinlichkeit:**  -

**Fallbeispiele:**  -

**Quellen:**

- http://www.datenschmutz.de/moin/Staatstrojaner


## Fähigkeit 16: RFID Chip

**Methode:** Ein Mini-Sender von der Größe von ein paar Millimetern, zusammen mit dem Sender hat das Platz auf einer Chipkarte. Ein RFID-Chip besitzt keine eigene Energiequelle, sondern bezieht seine Energie aus dem Sender, der mit ihm in Kontakt tritt (Genau beschrieben wird das auf der Spezialseite des Foebuds zum RFID-Chip). Der E-Perso ist mit einem RFID-Chip ausgerüstet. 

**Gefährdete Assets:** Position, Identität

**Hinweise zur Wahrscheinlichkeit:**  -

**Fallbeispiele:**  -

**Quellen:**

- http://www.datenschmutz.de/moin/Überwachungstechnik

## Fähigkeit 17: Einschreiben und Pakete

**Methode:** Ohne Kontrolle können die Geheimdienste ebenfalls auf die Datenbank der Post (siehe § 8a BverfSchGzugreifen, wo gespeichert ist wann, wo und an wen ein Einschreiben oder Paket abgeschickt wird. 

**Gefährdete Assets:** Kommunikation Metadaten

**Hinweise zur Wahrscheinlichkeit:**  -

**Fallbeispiele:**  -

**Quellen:**

- http://www.datenschmutz.de/moin/Überwachungstechnik


## Fähigkeit 18: Private-Datenbank

**Methode:** Viele Internet Firmen sammeln eine große Menge an Daten. Dazu gehören z.B. ebay, amazon, paypal, die Social Media Seiten usw.. Google sammelt laut einem Spiegel-Artikel: mehr Informationen über Internetnutzer als jedes andere Unternehmen. Seit vielen Jahren liefern Provider und Portalbetreiber den Ermittlungsbehörden oftmals ohne großen Widerstand alle möglichen Daten auf Anfrage hin. Das Spektrum reicht von Bestandsdaten bis hin zu konkreten Kommunikationsinhalten. Oft genug werden hierbei durch die Behörden auch die gesetzlichen Vorgaben nicht beachtet. In vielen Fällen genügt faktisch ein Fax einer Polizeidienststelle oder KPI mit einer “Auskunftsanfrage” und die Behörden bekommen die gewünschten Daten auf dem Silbertablett. 

**Gefährdete Assets:** Geheimnisse, Internet Verhalten

**Hinweise zur Wahrscheinlichkeit:**  -

**Fallbeispiele:**  -

**Quellen:**

- http://www.datenschmutz.de/moin/Überwachungstechnik
